# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require "rake"

Rake::TaskManager.record_task_metadata = true # keep before any task (:help)
PORTFOLIO_PATH = "#{__dir__}/_site"

task :default => ENV["JEKYLL_ENV"] == "production" ? :publish : :help

require "rake/clean"
CLOBBER.include [PORTFOLIO_PATH]

desc "Generate portfolio for publishing"
task :publish, [:directory, :environment] do |_, portfolio|
  portfolio.with_defaults directory: PORTFOLIO_PATH
  portfolio.with_defaults environment: ENV["JEKYLL_ENV"]

  puts "Publishing portfolio to #{portfolio.directory}"
  puts "for #{portfolio.environment}..."
  puts ""

  system(
    {"JEKYLL_ENV"=>portfolio.environment},
    *%W[
      jekyll build
      -d #{portfolio.directory}
    ]
  )
end

desc "Run local demo"
task :demo, [:port] do |_, localhost|
  localhost.with_defaults port: 4000 # jekyll's default

  puts ""
  puts "Starting server for http://localhost:#{localhost.port}"
  puts ""
  system({"JEKYLL_ENV"=>"development"}, *%W[jekyll serve --port #{localhost.port}])
end

desc "Show this list"
task :help do
  Rake::application.options.show_tasks = :tasks # :describe, :lines
  Rake::application.options.show_task_pattern = //
  Rake::application.display_tasks_and_comments
end
