source "https://rubygems.org"

gem "rake", "~> 13.0.1"
gem "jekyll", "~> 4.1.0"
gem "minitest", "~> 5.14.1"

group :jekyll_plugins do
  gem "jekyll-feed", "~> 0.12"
  gem "jekyll-org", "~> 1.1.1"
end
